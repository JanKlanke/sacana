---
title: "Preprocessing Script 1:  Edf conversion."
author: Jan Klanke
date: "`r Sys.date()`"
output: 
  pdf_document: default
---

```{r setup, include= FALSE}
knitr::opts_chunk$set( include= TRUE, size= 'footnotesize' )
knitr::opts_knit$set(  root.dir= '/Users/jan/Documents' )

# Change hook to be able to modify font size in chunks
def.source.hook <- knitr::knit_hooks$get( 'source' )
knitr::knit_hooks$set( source= function( x, options ) {
  x <- def.source.hook( x, options )
  ifelse( !is.null( options$size ), 
         paste0( '\\', options$size, '\n\n', x, '\n\n \\normalsize' ), 
         x ) } )
```
This script reads out edf files ands converts and splits them into a) MSG files that contain all the eyelink messages and b) DAT files comprising of time stamps, eye positions, and pupil sizes.

# Folder & format spec. parameter
This chunk entails all the parameter for the folder structure, file format specifiers, and so on.
```{r}
# Directory of subfolder with data. 
fld <- list()
fld$edf <- 'sacANA/edf/'             # (i.e. './Documents/ExpName/edf/') 

# Directory for files that have already been processed.
fld$prp <- 'dataMSGM/edf/processed'  # (i.e. './Documents/ExpName/edf/processed') 

# Directory of subfolder where the outputs will be stored.
fld$raw <- 'sacANA/raw/'            # (i.e. './Documents/ExpName/saccades/raw/') 

# Format specs of input and ouptu data (likely no need for change!).
fS <- list()
fS$edf <- '.edf'  # file fomrat of input data
fS$asc <- '.asc'  # file format of interim data
fS$msg <- '.msg'  # file format of output data
fS$dat <- '.dat'  # file format of ouptut data
```

# Set up optio  flags
In this chunk the file path to edf2asc conversion file is set, as well as the flags that specify the constraints of the conversion. The following flags are set:
-y overwrite asc file if exists (both files)
-e outputs event data only (MSG files)
-miss <value> replaces missing (x,y) in samples with <value>
-p <path> writes output with same name to <path> directory
```{r}
# complete filepath of edf2asc.exe
edf2asc <- 'edf2asc/edf2asc'     # (i.e. '/Documents/EDF2ASC/edf2asc')

# Edf2asc options settings.
edf2ascOpts <- list()
edf2ascOpts$msg <- paste( '-y', '-e',                  '-p', fld$raw )
edf2ascOpts$dat <- paste( '-y', '-s', '-miss', '-1.0', '-p', fld$raw )
```


# Transform EDF to MSG files
```{r}
# Translate edf to MSG files & rename files appropriately.
system( paste( edf2asc, edf2ascOpts$msg, paste0( fld$edf, '*' ) ) )
file.rename( from= list.files( fld$raw, paste0( '*', fS$asc ), full.names= TRUE ), 
             to= gsub( fS$asc, fS$msg, list.files( fld$raw, paste0( '*', fS$asc ), full.names= TRUE ) ) )
```

# Transform EDF to DAT files
```{r}
# Translate edf to DAT files & rename files appropriately.
system( paste( edf2asc, edf2ascOpts$dat, paste0( fld$edf, '*', fS$edf ) ) )
file.rename( from= list.files( fld$raw, paste0( '*', fS$asc ), full.names= TRUE ), 
             to= gsub( fS$asc, fS$dat, list.files( fld$raw, paste0( '*', fS$asc ), full.names= TRUE ) ) )

# Move files to processed directory.
fs::file_move( list.files( fld$edf, paste0( '*', fS$edf ), full.names= TRUE ), fld$done )
```
